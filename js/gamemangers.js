let GameManger = {
	setGameStart: function (classType) {
		this.resetPlayer(classType);
		this.setPrefight();
	},
	resetPlayer: function (classType) {
		//選角+設定角色
		switch (classType) {
			case "易文許":
				player = new Player(classType, 200, 0, 200, 100, 50);
				break;
			case "灰圣鵬":
				player = new Player(classType, 100, 0, 100, 150, 200);
				break;
			case "林晏鯉":
				player = new Player(classType, 80, 0, 50, 200, 50);
				break;
			case "庭雯娌":
				player = new Player(classType, 200, 0, 50, 200, 100);
				break;
				//			default:

		}
		//第二步(角色顯示)
		let getInterface = document.querySelector(".interface")
		getInterface.innerHTML =
			'<img src="img/player/' + classType.toLowerCase() + '.png" class="img-avatar"><div><h3>' + classType + '</h3><p class="health-player">Hp: ' + player.health + '</p><p>Mp: ' + player.mana + '</p><p>力量: ' + player.strength + '</p><p>敏捷: ' + player.agility + '</p><p>速度: ' + player.speed + '</p>';

	},
	setPrefight: function () {
		let getHeader = document.querySelector(".header");
		let getActions = document.querySelector(".actions");
		let getArena = document.querySelector(".arena");
		getHeader.innerHTML = '<p>今天上課時打瞌睡被老師發現，哀沒關西  去全聯買點好貨犒賞自己</p>';
		getActions.innerHTML = '<a href = "#" class="btn-prefight" onclick="GameManger.setFight()">去外面晃晃</a>';
		getArena.style.visibility = "visible";
	},
	setFight: function () {
		let getHeader = document.querySelector(".header");
		let getActions = document.querySelector(".actions");
		let getEnemy = document.querySelector(".enemy");
		//建造敵人
		let enemy00 = new Enemy("梅杜莎", 500, 0, 50, 100, 0);
		let enemy01 = new Enemy("蒼蠅男", 250, 0, 20, 80, 25);
		//選擇敵人(0~1)
		let chooseRandomEnemy = Math.floor(Math.random() * 2);
		//		console.log(chooseRandomEnemy);
		switch (chooseRandomEnemy) {
			case 0:
				enemy = enemy00;
				break;
			case 1:
				enemy = enemy01;
				break;
		}
		getHeader.innerHTML = '<p>好餓阿...</p>';
		getActions.innerHTML = '<a  class="btn-prefight" onclick="PlayerMoves.calcAttack()">發現敵人啦!!  ATTACK!</a>';
		getEnemy.innerHTML =
			'<img src="img/enemy/' + enemy.enemyType.toLowerCase() + '.png" class="img-avatar" onclick="PlayerMoves.calcAttack()"><div><h3>' + enemy.enemyType + '</h3><p class="health-enemy">Hp: ' + enemy.health + '</p><p>Mp: ' + enemy.mana + '</p><p>力量: ' + enemy.strength + '</p><p>敏捷: ' + enemy.agility + '</p><p>速度: ' + enemy.speed + '</p>';
	}
}
