let player;

//玩家資訊
function Player(classType, health, mana, strength, agility, speed) {
	this.classType = classType;
	this.health = health;
	this.mana = mana;
	this.strength = strength;
	this.agility = agility;
	this.speed = speed;
}

let PlayerMoves = {
	calcAttack: function () {
		//誰先攻擊
		let getPlayerSpeed = player.speed;
		let getEnemySpeed = enemy.speed;
		//玩家攻擊
		let playerAttack = function () {
			//基礎傷害
			let calcBaseDamage;
			//有魔
			if (player.mana > 0) {
				calcBaseDamage = Math.floor(player.strength * player.mana / 1000);
				//沒魔
			} else {
				calcBaseDamage = Math.floor(player.strength * player.agility / 1000);
			}
			//額外傷害(隨機傷害)
			let offsetDamage = Math.floor(Math.random() * 10);
			//傷害總和
			let calcOutputDamage = calcBaseDamage + offsetDamage;
			//攻擊次數
			let numberOfHits = Math.floor(Math.random() * (player.agility / 10) / 2) + 1;
			let attackValues = [calcOutputDamage, numberOfHits];
			return attackValues;
		} //敵人攻擊
		let enemyAttack = function () {
			//基礎傷害
			let calcBaseDamage;
			//有魔
			if (enemy.mana > 0) {
				calcBaseDamage = Math.floor(enemy.strength * enemy.mana / 1000);
				//沒魔
			} else {
				calcBaseDamage = Math.floor(enemy.strength * enemy.agility / 1000);
			}
			//額外傷害(隨機傷害0~9)
			let offsetDamage = Math.floor(Math.random() * 10);
			//傷害總和
			let calcOutputDamage = calcBaseDamage + offsetDamage;
			//攻擊次數
			let numberOfHits = Math.floor(Math.random() * (enemy.agility / 10) / 2) + 1;
			let attackValues = [calcOutputDamage, numberOfHits];
			return attackValues;
		}
		//取得血量並改寫
		let getPlayerHealth = document.querySelector(".health-player");
		let getEnemyHealth = document.querySelector(".health-enemy");
		//誰的速度快(玩家速度快情況)
		if (getPlayerSpeed >= getEnemySpeed) {
			let playerAttackValues = playerAttack();
			//傷害總和
			let totalDamage = playerAttackValues[0] * playerAttackValues[1];
			enemy.health = enemy.health - totalDamage;
			//顯示傷害
			//取得arena
			let getArena = document.querySelector(".arena");
			//arena內部顯示(新版)
			let new_option = new Option(player.classType + "對" + enemy.enemyType + "造成了" + playerAttackValues[0] + "傷害" + playerAttackValues[1] + "次", );
			getArena.options.add(new_option, getArena[0]);
			//arena內部顯示(舊版)
			//			getArena.innerHTML += "<option selected>" + player.classType + "對" + enemy.enemyType + "造成了" + playerAttackValues[0] + "傷害" + playerAttackValues[1] + "次</option>";
			//alert
			alert(player.classType + "對" + enemy.enemyType + "造成了" + playerAttackValues[0] + "傷害" + playerAttackValues[1] + "次");
			if (enemy.health <= 0) {
				alert("你贏了");
				getPlayerHealth.innerHTML = 'Health: ' + player.health;
				getEnemyHealth.innerHTML = 'Health: 0';

			} else {
				getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
				let enemyAttackValues = enemyAttack();
				let totalDamage = enemyAttackValues[0] * enemyAttackValues[1];
				player.health = player.health - totalDamage;
				//arena內部顯示(新版)
				let new_option = new Option(player.classType + "受到了" + enemyAttackValues[0] + "傷害" + enemyAttackValues[1] + "次" );
				getArena.options.add(new_option, getArena[0]);
				//alert
				alert(player.classType + "受到了" + enemyAttackValues[0] + "傷害" + enemyAttackValues[1] + "次" );
				getArena.options.add(new_option, getArena[0]);
				if (player.health <= 0) {
					alert("你輸了");
					getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
					getPlayerHealth.innerHTML = 'Health: 0';
				} else {
					getPlayerHealth.innerHTML = 'Health: ' + player.health;
				}
			}
		}
		//敵人比較快
		else if (getEnemySpeed > getPlayerSpeed) {
			let enemyAttackValues = enemyAttack();
			//傷害總和
			let totalDamage = enemyAttackValues[0] * enemyAttackValues[1];
			player.health = player.health - totalDamage;
			alert(player.classType + "受到了" + enemyAttackValues[0] + "傷害" + enemyAttackValues[1] + "次");
			if (player.health <= 0) {
				alert("你輸了");
				getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
				getPlayerHealth.innerHTML = 'Health: 0';
			} else {
				getPlayerHealth.innerHTML = 'Health: ' + player.health;
				let playerAttackValues = playerAttack();
				let totalDamage = playerAttackValues[0] * playerAttackValues[1];
				enemy.health = enemy.health - totalDamage;
				alert(player.classType + "對" + enemy.enemyType + "造成了" + playerAttackValues[0] + "傷害" + playerAttackValues[1] + "次");
				if (enemy.health <= 0) {
					alert("你贏了");
					getPlayerHealth.innerHTML = 'Health: ' + player.health;
					getEnemyHealth.innerHTML = 'Health: 0';

				} else {
					getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
				}
			}
		}


	}


}
